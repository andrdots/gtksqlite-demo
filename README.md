# gtksqlite-demo

## Description
The project is intended to demonstrate the gtkdbwidgets library and new approach to create tree widgets introduced in GTK4. The new approach uses combination of GtkListView and GtkColumnView to create separately the navigation tree and associated data table.

GSqliteProvider provides GDbProvider interface (actually it's an abstract class) to access SQLite database and fetch table data into the GDbRow items.

## Preparing
1. On Debian-based systems install dependency packages: git, build-essential, cmake, libsqlite3-dev and libgtk-4-dev:
```shell
sudo apt install -y git build-essential cmake libsqlite3-dev libgtk-4-dev
```

## Compiling
1. Clone sources of the project with submodules into a source directory and build the project build directory:
```shell
mkdir -p ~/db-view/build
cd ~/db-view
git clone --recurse-submodules https://gitlab.com/andrdots/gtksqlite-demo.git ~/db-view/source
cd ~/db-view/build && cmake -DCMAKE_INSTALL_PREFIX=/usr ../source/ && make
```
1. To build deb packages use command:
`cpack -G DEB`
1. To run the program type path to executable in the command line:
`./gtksqlite-demo`
1. Builtin GTK inspector can be used to debug graphical interface:
`GTK_DEBUG=interactive ./gtksqlite-demo`

## Authors and acknowledgment
* Andrey N. Dotsenko

## License
The source code is licensed under the LGPL 2.1 license.
