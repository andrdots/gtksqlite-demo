BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "tree_items" (
	"id"	INTEGER NOT NULL,
	"parent_id"	INTEGER DEFAULT NULL,
	FOREIGN KEY("id") REFERENCES "column_items"("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "column_items" (
	"id"	INTEGER NOT NULL UNIQUE,
	"title"	TEXT DEFAULT NULL,
	"description"	TEXT DEFAULT NULL,
	"article_id"	INTEGER DEFAULT NULL,
	"params"	TEXT DEFAULT NULL,
	"active"	INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY("id" AUTOINCREMENT)
);
INSERT INTO "tree_items" VALUES (1,NULL);
INSERT INTO "tree_items" VALUES (2,NULL);
INSERT INTO "tree_items" VALUES (3,NULL);
INSERT INTO "tree_items" VALUES (4,1);
INSERT INTO "tree_items" VALUES (5,1);
INSERT INTO "tree_items" VALUES (6,3);
INSERT INTO "tree_items" VALUES (7,NULL);
INSERT INTO "tree_items" VALUES (8,NULL);
INSERT INTO "tree_items" VALUES (9,1);
INSERT INTO "tree_items" VALUES (10,NULL);
INSERT INTO "tree_items" VALUES (11,NULL);
INSERT INTO "tree_items" VALUES (13,4);
INSERT INTO "tree_items" VALUES (14,4);
INSERT INTO "tree_items" VALUES (15,4);
INSERT INTO "tree_items" VALUES (16,7);
INSERT INTO "tree_items" VALUES (17,16);
INSERT INTO "tree_items" VALUES (18,16);
INSERT INTO "tree_items" VALUES (19,2);
INSERT INTO "tree_items" VALUES (20,12);
INSERT INTO "tree_items" VALUES (21,16);
INSERT INTO "tree_items" VALUES (22,2);
INSERT INTO "tree_items" VALUES (23,13);
INSERT INTO "tree_items" VALUES (24,13);
INSERT INTO "column_items" VALUES (1,'Название 1','Описание 1',44,'color:#FFF;',1);
INSERT INTO "column_items" VALUES (2,'Название 2','Описание 2',55,NULL,0);
INSERT INTO "column_items" VALUES (3,'Название 3','Описание 3',545,NULL,0);
INSERT INTO "column_items" VALUES (4,'Название 1.1','Описание 1.1',56456,'color:#F0F;',1);
INSERT INTO "column_items" VALUES (5,'Название 1.2','Описание 1.1',1,NULL,1);
INSERT INTO "column_items" VALUES (6,'Название 3.1',NULL,2,NULL,0);
INSERT INTO "column_items" VALUES (7,'Название 5',NULL,34,NULL,0);
INSERT INTO "column_items" VALUES (8,'Название 6',NULL,4,'color:#0FF;',0);
INSERT INTO "column_items" VALUES (9,'Название 1.3','Описание 1.3',55,NULL,1);
INSERT INTO "column_items" VALUES (10,'Название 7','Длинное описание 7',6,NULL,0);
INSERT INTO "column_items" VALUES (11,'Название 8','Очень длинное описание 8',7,'color:#FF6;',0);
INSERT INTO "column_items" VALUES (12,'Название 9',NULL,8,'',0);
INSERT INTO "column_items" VALUES (13,'Название 1.1.1',NULL,9,NULL,1);
INSERT INTO "column_items" VALUES (14,'Название 1.1.2','Невероятно длинне описание, которое сильно растягивает колонку.',10,'color:#000;',1);
INSERT INTO "column_items" VALUES (15,'Название 1.1.3',NULL,11,NULL,1);
INSERT INTO "column_items" VALUES (16,'Название 5.1',NULL,3434,NULL,0);
INSERT INTO "column_items" VALUES (17,'Название 5.1.1',NULL,343,NULL,0);
INSERT INTO "column_items" VALUES (18,'Название 5.1.2','5/1/2',434,'none',0);
INSERT INTO "column_items" VALUES (19,'Название 2.1',NULL,NULL,NULL,0);
INSERT INTO "column_items" VALUES (20,'Название 9.1',NULL,NULL,NULL,0);
INSERT INTO "column_items" VALUES (21,'Название 5.1.3',NULL,5453,NULL,0);
INSERT INTO "column_items" VALUES (22,'Название 2.1',NULL,NULL,NULL,0);
INSERT INTO "column_items" VALUES (23,'Название 1.1.1.1',NULL,NULL,NULL,1);
INSERT INTO "column_items" VALUES (24,'Название 1.1.1.2',NULL,NULL,NULL,1);
CREATE INDEX IF NOT EXISTS "tree_id" ON "tree_items" (
	"id"	ASC
);
CREATE INDEX IF NOT EXISTS "parent_id" ON "tree_items" (
	"parent_id"	ASC,
	"id"	ASC
);
CREATE UNIQUE INDEX IF NOT EXISTS "id" ON "column_items" (
	"id"	ASC
);
CREATE TRIGGER LinkTables 
AFTER INSERT ON column_items 
FOR EACH ROW 
BEGIN 
    INSERT INTO tree_items (id) 
    values (NEW.id); 
END;
COMMIT;
