/*
   Copyright (c) 2023 Dotsenko A. N.

   This file is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 2.1 of the
   License, or (at your option) any later version.

   This file is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gsqlite/gsqliteprovider.h>

#include <gtk/gtk.h>

typedef struct {
    GtkApplication *app;

    GDbProvider *db_provider;

    GListStore *tree_store;
    GListStore *columns_store;

    GtkWidget *tree_view;
    GtkWidget *column_view;
} main_ui_t;

main_ui_t main_ui;

guint col_id;
guint col_active;
guint col_title;
guint col_params;
guint col_article_id;
guint col_description;

static GListModel *create_list_model_cb(gpointer list_item, gpointer user_data)
{
    GDbRow *parent_row = list_item;
    int parent_id = g_db_row_get_int_column(parent_row, col_id);

    GListStore *list_store = g_list_store_new(G_TYPE_DB_ROW);

    char sql_query[512];
    g_snprintf(sql_query,
               sizeof(sql_query),
               "SELECT * from tree_items LEFT JOIN column_items ON "
               "tree_items.id = column_items.id WHERE "
               "tree_items.parent_id = %d",
               parent_id);

    GError *error = NULL;
    if (!g_db_provider_append_list_store_by_sql(
                main_ui.db_provider, sql_query, list_store, &error)) {
        g_critical("Unable to execute SQL sql_query `%s`: %s",
                   sql_query,
                   error->message);
        g_error_free(error);
        g_application_quit(G_APPLICATION(main_ui.app));
        return false;
    }

    if (g_list_model_get_n_items(G_LIST_MODEL(list_store)) == 0) {
        g_object_unref(list_store);
        return NULL;
    }

    return G_LIST_MODEL(list_store);
}

static void selection_changed_cb(GtkSingleSelection *selection_model,
                                 guint start_position,
                                 guint count,
                                 gpointer user_data)
{
    GtkTreeListRow *tree_row =
            gtk_single_selection_get_selected_item(selection_model);
    GDbRow *row = gtk_tree_list_row_get_item(tree_row);

    int id = g_db_row_get_int_column(row, col_id);

    g_list_store_remove_all(main_ui.columns_store);

    GError *error = NULL;
    char sql_query[256];
    g_snprintf(
            sql_query,
            sizeof(sql_query),
            "SELECT * from column_items RIGHT JOIN tree_items ON "
            "column_items.id = tree_items.id WHERE tree_items.parent_id = %d",
            id);
    if (!g_db_provider_append_list_store_by_sql(main_ui.db_provider,
                                                sql_query,
                                                main_ui.columns_store,
                                                &error)) {
        g_critical("Unable to execute SQL sql_query `%s`: %s",
                   sql_query,
                   error->message);
        g_error_free(error);
        return;
    }
}

static void
listview_activate_cb(GtkListView *list, guint position, gpointer user_data)
{
    GListModel *list_model = G_LIST_MODEL(gtk_list_view_get_model(list));
    GtkTreeListRow *tree_row = g_list_model_get_item(list_model, position);
    gtk_tree_list_row_set_expanded(tree_row,
                                   !gtk_tree_list_row_get_expanded(tree_row));
}

static gboolean init_db(void)
{
    main_ui.db_provider = gtk_sqlite_provider_new();

    const char *db_path =
            g_strdup_printf("%s/gtksqlite-demo.db", g_get_user_config_dir());

    gboolean db_exists = g_file_test(db_path, G_FILE_TEST_EXISTS);

    GError *error = NULL;
    if (!g_db_provider_open_db(main_ui.db_provider, db_path, &error)) {
        g_critical("Unable to open database `%s`: %s", db_path, error->message);
        g_error_free(error);
        g_application_quit(G_APPLICATION(main_ui.app));
        return false;
    }

    col_id = g_db_provider_get_id_by_col_name(main_ui.db_provider, "id");
    col_active =
            g_db_provider_get_id_by_col_name(main_ui.db_provider, "active");
    col_title = g_db_provider_get_id_by_col_name(main_ui.db_provider, "title");
    col_description = g_db_provider_get_id_by_col_name(main_ui.db_provider,
                                                       "description");
    col_params =
            g_db_provider_get_id_by_col_name(main_ui.db_provider, "params");
    col_article_id =
            g_db_provider_get_id_by_col_name(main_ui.db_provider, "article_id");

    if (!db_exists) {
        gchar *sql_query = NULL;
        const char *db_sql_path =
                "/usr/share/gtksqlite-demo/gtksqlite-demo.db.sql";
        if (!g_file_test(db_sql_path, G_FILE_TEST_EXISTS)) {
            db_sql_path = "./gtksqlite-demo.db.sql";
        }
        if (!g_file_get_contents(db_sql_path, &sql_query, NULL, &error)) {
            g_critical("Unable to load database SQL (%s): %s",
                       db_path,
                       error->message);
            return false;
        }
        if (!g_db_provider_exec_sql(main_ui.db_provider, sql_query, &error)) {
            g_critical("Unable to execute database SQL (%s): %s",
                       db_path,
                       error->message);
            return false;
        }
    }

    main_ui.tree_store = g_list_store_new(G_TYPE_DB_ROW);

    const char *sql_query =
            "SELECT * from tree_items LEFT JOIN column_items ON "
            "tree_items.id = column_items.id WHERE "
            "tree_items.parent_id is NULL";
    if (!g_db_provider_append_list_store_by_sql(
                main_ui.db_provider, sql_query, main_ui.tree_store, &error)) {
        g_critical("Unable to execute SQL sql_query `%s`: %s",
                   sql_query,
                   error->message);
        g_error_free(error);
        g_application_quit(G_APPLICATION(main_ui.app));
        return false;
    }

    return true;
}

static void tree_list_item_setup_cb(GtkListItemFactory *factory,
                                    GtkListItem *list_item,
                                    gpointer user_data)
{
    GtkWidget *tree_expander = gtk_tree_expander_new();
    gtk_list_item_set_child(list_item, tree_expander);

    GtkWidget *box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);

    GtkWidget *checkbutton = gtk_check_button_new();
    gtk_box_append(GTK_BOX(box), checkbutton);

    GtkWidget *label = gtk_label_new(NULL);
    gtk_box_append(GTK_BOX(box), label);

    gtk_tree_expander_set_child(GTK_TREE_EXPANDER(tree_expander), box);
}

static void set_tree_branch_active(GtkTreeListRow *tree_row, gboolean new_state)
{
    GListModel *children = gtk_tree_list_row_get_children(tree_row);
    if (!children) {
        return;
    }

    for (guint i = 0; i < g_list_model_get_n_items(children); ++i) {
        GDbRow *curr_row = g_list_model_get_item(children, i);
        g_db_row_set_int_column(curr_row, col_active, new_state);

        GtkTreeListRow *curr_tree_row =
                gtk_tree_list_row_get_child_row(tree_row, i);
        set_tree_branch_active(curr_tree_row, new_state);
        g_object_unref(curr_tree_row);
    }
}

static void set_parents_active(GtkTreeListRow *tree_row, gboolean new_state)
{
    char sql_query[512];
    GError *error = NULL;

    if (!new_state) {
        GtkTreeListRow *parent_tree_row =
                gtk_tree_list_row_get_parent(tree_row);
        while (parent_tree_row) {
            GDbRow *parent_row = gtk_tree_list_row_get_item(parent_tree_row);
            int parent_id = g_db_row_get_int_column(parent_row, col_id);
            g_snprintf(sql_query,
                       sizeof(sql_query),
                       "UPDATE column_items SET active = %d WHERE id = %d",
                       new_state,
                       parent_id);
            if (!g_db_provider_exec_sql(
                        main_ui.db_provider, sql_query, &error)) {
                g_critical("Unable to execute SQL sql_query `%s`: %s",
                           sql_query,
                           error->message);
                return;
            } else {
                g_db_row_set_int_column(parent_row, col_active, false);
            }

            g_object_unref(parent_tree_row);
            parent_tree_row = gtk_tree_list_row_get_parent(parent_tree_row);
        }
    } else {
        GtkTreeListRow *parent_tree_row =
                gtk_tree_list_row_get_parent(tree_row);
        while (parent_tree_row) {
            GListModel *subling_tree_rows =
                    gtk_tree_list_row_get_children(parent_tree_row);
            gboolean sublings_active = true;
            for (int i = 0; i < g_list_model_get_n_items(subling_tree_rows);
                 ++i) {
                GDbRow *curr_row = g_list_model_get_item(subling_tree_rows, i);
                gboolean active = g_db_row_get_int_column(curr_row, col_active);
                if (!active) {
                    sublings_active = false;
                    break;
                }
            }
            if (sublings_active) {
                GDbRow *parent_row =
                        gtk_tree_list_row_get_item(parent_tree_row);
                int parent_id = g_db_row_get_int_column(parent_row, col_id);
                g_snprintf(sql_query,
                           sizeof(sql_query),
                           "UPDATE column_items SET active = %d WHERE id = %d",
                           true,
                           parent_id);
                if (!g_db_provider_exec_sql(
                            main_ui.db_provider, sql_query, &error)) {
                    g_critical("Unable to execute SQL sql_query `%s`: %s",
                               sql_query,
                               error->message);
                    return;
                } else {
                    g_db_row_set_int_column(parent_row, col_active, true);
                }
            }

            g_object_unref(parent_tree_row);
            parent_tree_row = gtk_tree_list_row_get_parent(parent_tree_row);
        }
    }
}

typedef struct {
    GDbRow *row;
    GtkTreeListRow *tree_row;
    GtkWidget *check_box;
    gulong checkbox_handler;
    gulong row_handler;
} RowChangedCbCata;

static void checkbutton_toggled_data_free_cb(gpointer data, GClosure *closure)
{
    RowChangedCbCata *cb_data = data;
    g_signal_handler_disconnect(cb_data->row, cb_data->row_handler);
    g_free(cb_data);
}

static void checkbutton_toggled_cb(GtkToggleButton *self, gpointer user_data)
{
    RowChangedCbCata *cb_data = user_data;
    GtkTreeListRow *tree_row = cb_data->tree_row;
    GDbRow *row = gtk_tree_list_row_get_item(tree_row);
    int id = g_db_row_get_int_column(row, col_id);
    gboolean active = g_db_row_get_int_column(row, col_active);
    gboolean new_state = !active;

    // Recursive sql_query using CTE. We need to walk through every item,
    // because the deepest item can have either state.
    char sql_query[512];
    g_snprintf(sql_query,
               sizeof(sql_query),
               "WITH RECURSIVE branch(id, active) AS"
               "("
               "    SELECT id, active FROM column_items WHERE id = %d"
               "    UNION ALL"
               "    SELECT tree_items.id, column_items.active FROM "
               "column_items JOIN tree_items ON column_items.id = "
               "tree_items.id JOIN branch ON branch.id = tree_items.parent_id"
               ")"
               "UPDATE column_items SET active = %d FROM branch WHERE "
               "column_items.id = branch.id",
               id,
               new_state);
    GError *error = NULL;
    if (!g_db_provider_exec_sql(main_ui.db_provider, sql_query, &error)) {
        g_critical("Unable to execute SQL sql_query `%s`: %s",
                   sql_query,
                   error->message);
        return;
    } else {
        g_db_row_set_int_column(row, col_active, new_state);
    }

    // Update child nodes, too:
    set_tree_branch_active(tree_row, new_state);

    set_parents_active(tree_row, new_state);
}

static void row_changed_cb(GDbRow *row, gpointer user_data)
{
    RowChangedCbCata *cb_data = user_data;
    bool active = g_db_row_get_int_column(row, col_active);
    if (active !=
        gtk_check_button_get_active(GTK_CHECK_BUTTON(cb_data->check_box))) {
        g_signal_handler_block(cb_data->check_box, cb_data->checkbox_handler);
        gtk_check_button_set_active(GTK_CHECK_BUTTON(cb_data->check_box),
                                    active);
        g_signal_handler_unblock(cb_data->check_box, cb_data->checkbox_handler);
    }
}

static void tree_list_item_bind_cb(GtkListItemFactory *factory,
                                   GtkListItem *list_item,
                                   gpointer user_data)
{
    GtkTreeListRow *tree_row = gtk_list_item_get_item(list_item);
    GtkWidget *tree_expander = gtk_list_item_get_child(list_item);

    gtk_tree_expander_set_list_row(GTK_TREE_EXPANDER(tree_expander), tree_row);

    GtkWidget *box =
            gtk_tree_expander_get_child(GTK_TREE_EXPANDER(tree_expander));
    GtkWidget *label = gtk_widget_get_last_child(box);

    GDbRow *row = gtk_tree_list_row_get_item(tree_row);

    const GValue *value = g_db_row_get_value(row, col_title);
    const gchar *value_text;
    if (value) {
        value_text = g_value_get_string(value);
    } else {
        value_text = NULL;
    }
    gtk_label_set_label(GTK_LABEL(label), value_text);

    GtkWidget *checkbox = gtk_widget_get_first_child(box);

    value = g_db_row_get_value(row, col_active);
    if (value) {
        gtk_check_button_set_active(GTK_CHECK_BUTTON(checkbox),
                                    g_value_get_int(value));
    } else {
        gtk_check_button_set_active(GTK_CHECK_BUTTON(checkbox), 0);
    }

    RowChangedCbCata *cb_data = g_new(RowChangedCbCata, 1);
    cb_data->row = row;
    cb_data->tree_row = tree_row;
    cb_data->check_box = checkbox;

    cb_data->checkbox_handler =
            g_signal_connect_data(checkbox,
                                  "toggled",
                                  G_CALLBACK(checkbutton_toggled_cb),
                                  cb_data,
                                  checkbutton_toggled_data_free_cb,
                                  G_CONNECT_DEFAULT);

    cb_data->row_handler = g_signal_connect(
            row, "changed", G_CALLBACK(row_changed_cb), cb_data);
}

static void tree_list_item_unbind_cb(GtkSignalListItemFactory *factory,
                                     GtkListItem *list_item,
                                     gpointer user_data)
{
    GtkWidget *tree_expander = gtk_list_item_get_child(list_item);

    GtkWidget *box =
            gtk_tree_expander_get_child(GTK_TREE_EXPANDER(tree_expander));

    GtkWidget *checkbox = gtk_widget_get_first_child(box);

    g_signal_handlers_disconnect_matched(checkbox,
                                         G_SIGNAL_MATCH_FUNC,
                                         0,
                                         0,
                                         NULL,
                                         G_CALLBACK(checkbutton_toggled_cb),
                                         NULL);
}

static void init_tree_view(void)
{
    GtkListItemFactory *tree_factory = gtk_signal_list_item_factory_new();
    g_signal_connect(
            tree_factory, "setup", G_CALLBACK(tree_list_item_setup_cb), NULL);
    g_signal_connect(
            tree_factory, "bind", G_CALLBACK(tree_list_item_bind_cb), NULL);
    g_signal_connect(
            tree_factory, "unbind", G_CALLBACK(tree_list_item_unbind_cb), NULL);

    GtkTreeListModel *model =
            gtk_tree_list_model_new(G_LIST_MODEL(main_ui.tree_store),
                                    FALSE,
                                    FALSE,
                                    create_list_model_cb,
                                    NULL,
                                    NULL);

    GtkSingleSelection *tree_view_selection =
            gtk_single_selection_new(G_LIST_MODEL(model));

    main_ui.tree_view = gtk_list_view_new(
            GTK_SELECTION_MODEL(tree_view_selection), tree_factory);
    gtk_widget_set_visible(main_ui.tree_view, true);
    g_signal_connect(main_ui.tree_view,
                     "activate",
                     G_CALLBACK(listview_activate_cb),
                     NULL);
    g_signal_connect(tree_view_selection,
                     "selection-changed",
                     G_CALLBACK(selection_changed_cb),
                     NULL);
}

static void column_list_item_setup_cb(GtkListItemFactory *factory,
                                      GtkListItem *list_item,
                                      gpointer user_data)
{
    GtkWidget *label = gtk_label_new(NULL);
    gtk_list_item_set_child(list_item, label);
}

static void column_list_item_bind_cb(GtkListItemFactory *factory,
                                     GtkListItem *list_item,
                                     gpointer user_data)
{
    guint col_num = GPOINTER_TO_UINT(user_data);
    GtkWidget *child = gtk_list_item_get_child(list_item);
    GDbRow *item = gtk_list_item_get_item(list_item);

    const GValue *value = g_db_row_get_value(item, col_num);
    char buf[16];
    const gchar *value_as_text;
    if (value) {
        if (G_VALUE_TYPE(value) == G_TYPE_STRING) {
            value_as_text = g_value_get_string(value);
        } else if (G_VALUE_TYPE(value) == G_TYPE_INT) {
            g_snprintf(buf, sizeof(buf), "%d", g_value_get_int(value));
            value_as_text = buf;
        } else {
            value_as_text = NULL;
        }
    } else {
        value_as_text = NULL;
    }
    gtk_label_set_text(GTK_LABEL(child), value_as_text);
}

static void init_column_view(void)
{
    GtkListItemFactory *title_factory = gtk_signal_list_item_factory_new();
    g_signal_connect(title_factory,
                     "setup",
                     G_CALLBACK(column_list_item_setup_cb),
                     NULL);
    g_signal_connect(title_factory,
                     "bind",
                     G_CALLBACK(column_list_item_bind_cb),
                     GUINT_TO_POINTER(col_title));

    GtkColumnViewColumn *column_title =
            gtk_column_view_column_new("Название", title_factory);

    GtkListItemFactory *description_factory =
            gtk_signal_list_item_factory_new();
    g_signal_connect(description_factory,
                     "setup",
                     G_CALLBACK(column_list_item_setup_cb),
                     NULL);
    g_signal_connect(description_factory,
                     "bind",
                     G_CALLBACK(column_list_item_bind_cb),
                     GUINT_TO_POINTER(col_description));

    GtkColumnViewColumn *column_description =
            gtk_column_view_column_new("Описание", description_factory);
    gtk_column_view_column_set_expand(column_description, true);

    GtkListItemFactory *params_factory = gtk_signal_list_item_factory_new();
    g_signal_connect(params_factory,
                     "setup",
                     G_CALLBACK(column_list_item_setup_cb),
                     NULL);
    g_signal_connect(params_factory,
                     "bind",
                     G_CALLBACK(column_list_item_bind_cb),
                     GUINT_TO_POINTER(col_params));

    GtkColumnViewColumn *column_params =
            gtk_column_view_column_new("Параметры", params_factory);

    GtkListItemFactory *article_id_factory = gtk_signal_list_item_factory_new();
    g_signal_connect(article_id_factory,
                     "setup",
                     G_CALLBACK(column_list_item_setup_cb),
                     NULL);
    g_signal_connect(article_id_factory,
                     "bind",
                     G_CALLBACK(column_list_item_bind_cb),
                     GUINT_TO_POINTER(col_article_id));

    GtkColumnViewColumn *column_article_id =
            gtk_column_view_column_new("Артикул", article_id_factory);

    GtkSingleSelection *columns_view_selection =
            gtk_single_selection_new(G_LIST_MODEL(main_ui.columns_store));

    main_ui.column_view =
            gtk_column_view_new(GTK_SELECTION_MODEL(columns_view_selection));
    gtk_widget_set_hexpand(main_ui.column_view, true);
    gtk_widget_set_visible(main_ui.column_view, true);

    gtk_column_view_append_column(GTK_COLUMN_VIEW(main_ui.column_view),
                                  column_title);
    gtk_column_view_append_column(GTK_COLUMN_VIEW(main_ui.column_view),
                                  column_description);
    gtk_column_view_append_column(GTK_COLUMN_VIEW(main_ui.column_view),
                                  column_params);
    gtk_column_view_append_column(GTK_COLUMN_VIEW(main_ui.column_view),
                                  column_article_id);
}

static void app_activate(GtkApplication *app, gpointer user_data)
{
    GtkWidget *window = gtk_application_window_new(app);
    gtk_widget_set_visible(window, true);
    gtk_window_set_default_size(GTK_WINDOW(window), 600, 400);

    GtkCssProvider *cssprovider = gtk_css_provider_new();
    const char *css_file = "/usr/share/gtksqlite-demo/db-view.css";
    if (!g_file_test(css_file, G_FILE_TEST_EXISTS)) {
        css_file = "./db-view.css";
    }
    gtk_css_provider_load_from_path(cssprovider, css_file);
    gtk_style_context_add_provider_for_display(
            gdk_display_get_default(),
            GTK_STYLE_PROVIDER(cssprovider),
            GTK_STYLE_PROVIDER_PRIORITY_USER);

    main_ui.columns_store = g_list_store_new(G_TYPE_DB_ROW);

    if (!init_db()) {
        return;
    }

    init_tree_view();
    init_column_view();

    GtkWidget *root_paned = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);

    GtkWidget *scrolledwindow_treeview = gtk_scrolled_window_new();
    gtk_scrolled_window_set_child(GTK_SCROLLED_WINDOW(scrolledwindow_treeview),
                                  main_ui.tree_view);
    gtk_scrolled_window_set_min_content_width(
            GTK_SCROLLED_WINDOW(scrolledwindow_treeview), 200);
    gtk_paned_set_start_child(GTK_PANED(root_paned), scrolledwindow_treeview);

    GtkWidget *scrolledwindow_columnview = gtk_scrolled_window_new();
    gtk_scrolled_window_set_child(
            GTK_SCROLLED_WINDOW(scrolledwindow_columnview),
            main_ui.column_view);
    gtk_scrolled_window_set_min_content_width(
            GTK_SCROLLED_WINDOW(scrolledwindow_columnview), 400);
    gtk_paned_set_end_child(GTK_PANED(root_paned), scrolledwindow_columnview);

    gtk_widget_set_visible(root_paned, true);
    gtk_window_set_child(GTK_WINDOW(window), root_paned);
}

int main(int argc, char *argv[])
{
    main_ui.app = gtk_application_new("org.gtk.sqlite-demo",
                                      G_APPLICATION_DEFAULT_FLAGS);

    g_signal_connect(main_ui.app, "activate", G_CALLBACK(app_activate), NULL);
    g_application_run(G_APPLICATION(main_ui.app), argc, argv);
    g_object_unref(main_ui.db_provider);
    g_object_unref(main_ui.app);
}
